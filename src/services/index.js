import axios from "./Api";

const getTypes = () => {
  return axios.get("core/data/third-car-types").then((res) => res.data);
};

const getDiscount = () => {
  return axios.get("core/data/car-third-discount").then((res) => res.data);
};

const getCompanies = () => {
  return axios.get("core/data/companies").then((res) => res.data);
};

export { getTypes, getDiscount, getCompanies };
