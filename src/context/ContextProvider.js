import React, { createContext, useReducer } from "react";
export const MainContext = createContext();

const initialState = {
  user: {},
  car: "",
  carModel: "",
  company: "",
  discount: "",
  discountAccident: "",
  modal: false,
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case "REGISTER":
      return {
        ...state,
        user: {
          name: payload.name,
          last_name: payload.last_name,
          phone: payload.phone,
          password: payload.password,
        },
      };
    case "TYPE":
      return {
        ...state,
        car: payload.car,
        carModel: payload.carModel,
      };
    case "COMPANY":
      return {
        ...state,
        company: payload.company,
      };
    case "DISCOUNT":
      return {
        ...state,
        discount: payload.discount,
        discountAccident: payload.discountAccident,
        modal: true,
      };
    case "MODAL":
      return {
        ...state,
        modal: !state.modal,
      };
    default:
      return state;
  }
};
const MainContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <MainContext.Provider value={{ state, dispatch }}>
      {children}
    </MainContext.Provider>
  );
};

export default MainContextProvider;
