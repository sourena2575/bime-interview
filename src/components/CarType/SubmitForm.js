import { useContext, useState } from "react";
import { MainContext } from "../../context/ContextProvider";

const RenderProps = ({ children }) => {
  const { dispatch } = useContext(MainContext);
  const [car, setcar] = useState("سواری");
  const [carModel, setcarModel] = useState("");
  return children({
    car: car,
    setcar: setcar,
    carModel: carModel,
    setcarModel: setcarModel,
    dispatch: dispatch,
  });
};

export default RenderProps;
