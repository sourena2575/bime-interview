import React from "react";
import { Row, Col } from "reactstrap";
import SubmitForm from "./SubmitForm";
import { LeftIcon, RightIcon } from "../common/Icons";

const Form = ({ types, history }) => {
  return (
    <SubmitForm>
      {({ car, setcar, carModel, setcarModel, dispatch }) => (
        <>
          <h5 className="font-weight-bold text-right py-4">بیمه شخص ثالث</h5>
          <h6 className="text-right text-muted pb-4">
            نوع و مدل خودروی خود را انتخاب کنید
          </h6>
          <Row>
            <Col xs="6">
              <select
                className="form-control"
                style={{ direction: "rtl" }}
                onChange={(e) => setcarModel(e.target.value)}
              >
                <option value="">مدل خودرو</option>
                {types
                  .find((it) => it.carType === car)
                  .brand.map((i) => {
                    return (
                      <option key={i.id} value={i.name}>
                        {i.name}
                      </option>
                    );
                  })}
              </select>
            </Col>
            <Col xs="6">
              <select
                className="form-control"
                style={{ direction: "rtl" }}
                onChange={(e) => setcar(e.target.value)}
              >
                <option value="">نوع خودرو</option>
                {types.map((item) => {
                  return (
                    <option key={item.carTypeID} value={item.carType}>
                      {item.carType}
                    </option>
                  );
                })}
              </select>
            </Col>
          </Row>
          <Row className="my-5">
            <Col xs="6">
              <button
                className="w-50 ml-auto btn btn-outline-info"
                style={{
                  borderColor: "##25b79b",
                  color: "#25b79b",
                  borderRadius: 20,
                  borderWidth: 1,
                }}
                onClick={
                  car && carModel
                    ? () => {
                        dispatch({ type: "TYPE", payload: { car, carModel } });
                        history.push("/company");
                      }
                    : null
                }
              >
                <LeftIcon />
                مرحله بعد
              </button>
            </Col>
            <Col xs="6">
              <button
                className="w-50 float-right btn btn-outline-info"
                style={{
                  borderColor: "##25b79b",
                  color: "#25b79b",
                  borderRadius: 20,
                  borderWidth: 1,
                }}
                onClick={() => history.push("/select")}
              >
                بازگشت
                <RightIcon />
              </button>
            </Col>
          </Row>
        </>
      )}
    </SubmitForm>
  );
};

export default Form;
