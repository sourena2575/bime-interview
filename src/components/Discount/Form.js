import React from "react";
import { Row, Col } from "reactstrap";
import SubmitForm from "./SubmitForm";

const Form = ({ data }) => {
  return (
    <SubmitForm>
      {({
        discount,
        setdiscount,
        discountAccident,
        setdiscountAccident,
        dispatch,
      }) => (
        <>
          <h5 className="font-weight-bold text-right py-4">بیمه شخص ثالث</h5>
          <h6 className="text-right text-muted pb-4">
            درصد تخفیف بیمه شخص ثالث و حوادث رانندگی را وارد کنید
          </h6>
          <Row>
            <Col xs="12" className="my-3">
              <select
                className="form-control"
                style={{ direction: "rtl" }}
                onChange={(e) => setdiscount(e.target.value)}
              >
                <option value="">درصد تخفیف ثالث </option>
                {data.map((item) => {
                  return (
                    <option key={item.id} value={item.title}>
                      {item.title}
                    </option>
                  );
                })}
              </select>
            </Col>
            <Col xs="12" className="my-3">
              <select
                className="form-control"
                style={{ direction: "rtl" }}
                onChange={(e) => setdiscountAccident(e.target.value)}
              >
                <option value="">درصد تخفیف حوادث راننده</option>
                {data.map((item) => {
                  return (
                    <option key={item.id} value={item.title}>
                      {item.title}
                    </option>
                  );
                })}
              </select>
            </Col>
          </Row>
          <Row className="my-5">
            <Col xs="6">
              <button
                style={{ borderRadius: 20, backgroundColor: "#25b79b" }}
                className="btn btn-block w-50 btn-success mr-auto"
                onClick={
                  discountAccident && discountAccident
                    ? () =>
                        dispatch({
                          type: "DISCOUNT",
                          payload: { discount, discountAccident },
                        })
                    : null
                }
              >
                استعلام قیمت
              </button>
            </Col>
            <Col xs="6"></Col>
          </Row>
        </>
      )}
    </SubmitForm>
  );
};

export default Form;
