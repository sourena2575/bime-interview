import { useContext, useState } from "react";
import { MainContext } from "../../context/ContextProvider";

const RenderProps = ({ children }) => {
  const { dispatch } = useContext(MainContext);
  const [discount, setdiscount] = useState("");
  const [discountAccident, setdiscountAccident] = useState("");

  return children({
    discount: discount,
    setdiscount: setdiscount,
    discountAccident: discountAccident,
    setdiscountAccident: setdiscountAccident,
    dispatch: dispatch,
  });
};

export default RenderProps;
