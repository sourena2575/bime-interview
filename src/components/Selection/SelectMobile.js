import React from "react";
import { Row, Col, Container } from "reactstrap";
import { CarIcon } from "../common/Icons";

const Select = ({ history }) => {
  return (
    <Container>
      <h5 className="font-weight-bold text-right py-4"> انتخاب بیمه</h5>
      <Row>
        <Col xs="8" className="my-3 mx-auto">
          <div
            className="border py-3 d-flex flex-column align-items-center justify-content-center"
            style={{ borderRadius: 18, cursor: "pointer" }}
            onClick={() => history.push("/type")}
          >
            <CarIcon />
            <p className="text-center font-weight-bold h6">شخص ثالث</p>
          </div>
        </Col>
        <Col xs="8" className="my-3 mx-auto">
          <div
            className="border py-3 d-flex flex-column align-items-center justify-content-center"
            style={{ borderRadius: 18, opacity: 0.5 }}
          >
            <CarIcon />
            <p className="text-center font-weight-bold h6"> بدنه</p>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default Select;
