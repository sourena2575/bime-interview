import React from "react";
import { Row, Col } from "reactstrap";
import { CarIcon } from "../common/Icons";
const Select = ({ history }) => {
  return (
    <>
      <h5 className="font-weight-bold text-right py-4"> انتخاب بیمه</h5>
      <Row>
        <Col xs="2"></Col>
        <Col xs="5" className="my-3">
          <div
            className="border py-3 d-flex flex-column align-items-center justify-content-center"
            style={{ borderRadius: 18, opacity: 0.5 }}
          >
            <CarIcon />
            <p className="text-center font-weight-bold h6"> بدنه</p>
          </div>
        </Col>
        <Col xs="5" className="my-3">
          <div
            className="border py-3 d-flex flex-column align-items-center justify-content-center"
            style={{ borderRadius: 18, cursor: "pointer" }}
            onClick={() => history.push("/type")}
          >
            <CarIcon />
            <p className="text-center font-weight-bold h6">شخص ثالث</p>
          </div>
        </Col>
      </Row>
    </>
  );
};

export default Select;
