const persianRegex = /^[\u0600-\u06FF\s]+$/;
const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{4,10}$/;
const mobileRegex = /(\+98|0)?9\d{9}/;
export { persianRegex, passwordRegex, mobileRegex };
