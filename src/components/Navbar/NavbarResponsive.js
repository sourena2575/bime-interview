import React, { useState, useContext } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  Container,
} from "reactstrap";
import { MainContext } from "../../context/ContextProvider";

const NavbarFile = () => {
  const { state } = useContext(MainContext);
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <Navbar color="light" light expand="md">
      <Container>
        <NavbarBrand>
          {state.user.name
            ? `${state.user.name} ${state.user.last_name}`
            : "ثبت نام"}
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mx-auto" navbar>
            <NavItem>
              <p className="font-weight-bold h6 pt-2">
                سامانه مقایسه و خرید آنلاین بیمه
              </p>
            </NavItem>
            <NavItem></NavItem>
          </Nav>
        </Collapse>
      </Container>
    </Navbar>
  );
};

export default NavbarFile;
