import React, { useRef, useContext } from "react";
import { Container, Row, Alert, Form } from "reactstrap";
import { useFormik } from "formik";
import { MainContext } from "../../../context/ContextProvider";
import {
  persianRegex,
  mobileRegex,
  passwordRegex,
} from "../../common/RegularExpresssion";

const RegisterForm = ({ history }) => {
  const { dispatch } = useContext(MainContext);
  const nameRef = useRef(null);
  const last_nameRef = useRef(null);
  const phoneRef = useRef(null);
  const passRef = useRef(null);

  const initialValues = {
    name: "",
    last_name: "",
    phone: "",
    password: "",
  };
  const onSubmit = (values) => {
    dispatch({ type: "REGISTER", payload: values });
    history.push("/select");
  };
  const validate = (values) => {
    let errors = {};
    if (!values.name) {
      errors.name = "  این ردیف الزامی است";
    }
    if (!values.last_name) {
      errors.last_name = "  این ردیف الزامی است";
    }
    if (!values.phone) {
      errors.phone = "  این ردیف الزامی است";
    } else if (!values.phone.match(mobileRegex)) {
      errors.phone = "  فرمت وارد شده صحیح نمی باشد   ";
    }
    if (!values.password) {
      errors.password = "  این ردیف الزامی است";
    } else if (!values.password.match(passwordRegex)) {
      errors.password =
        "رمز باید حد اقل شامل یک حرف بزرگ و یک حرف کوچک باشد، طول رمز باید بین چهار تا ده کاراکتر باشد";
    }

    return errors;
  };
  const formik = useFormik({
    initialValues,
    onSubmit,
    validate,
  });

  return (
    <Container>
      <h5 className="font-weight-bold text-center pb-4">ثبت نام</h5>
      <Form onSubmit={formik.handleSubmit}>
        <Row className="d-flex flex-row-reverse">
          <div className="col-12  my-2">
            {formik.errors.name && formik.touched.name ? (
              <Alert
                className="text-right my-1"
                color="danger"
                style={{ fontSize: 10 }}
              >
                {formik.errors.name}
              </Alert>
            ) : null}
            <input
              type="text"
              name="name"
              placeholder=" نام  "
              ref={nameRef}
              className="text-right form-control"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              onInput={() =>
                (nameRef.current.value = nameRef.current.value.match(
                  persianRegex
                )
                  ? nameRef.current.value
                  : "")
              }
            />
          </div>
          <div className="col-12  my-2">
            {formik.errors.last_name && formik.touched.last_name ? (
              <Alert
                className="text-right my-1"
                color="danger"
                style={{ fontSize: 10 }}
              >
                {formik.errors.last_name}
              </Alert>
            ) : null}
            <input
              type="text"
              name="last_name"
              placeholder="نام خانوادگی  "
              ref={last_nameRef}
              className="text-right form-control"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
              onInput={() =>
                (last_nameRef.current.value = last_nameRef.current.value.match(
                  persianRegex
                )
                  ? last_nameRef.current.value
                  : "")
              }
            />
          </div>
          <div className="col-12  my-2">
            {formik.errors.phone && formik.touched.phone ? (
              <Alert
                className="text-right my-1"
                color="danger"
                style={{ fontSize: 10 }}
              >
                {formik.errors.phone}
              </Alert>
            ) : null}
            <input
              type="text"
              placeholder=" شماره موبایل  "
              name="phone"
              ref={phoneRef}
              className="text-right form-control"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
            />
          </div>
          <div className="col-12  my-2">
            {formik.errors.password && formik.touched.password ? (
              <Alert
                className="text-right my-1"
                color="danger"
                style={{ fontSize: 10 }}
              >
                {formik.errors.password}
              </Alert>
            ) : null}
            <input
              type="text"
              placeholder=" رمز عبور  "
              name="password"
              ref={passRef}
              className="text-right form-control"
              onBlur={formik.handleBlur}
              onChange={formik.handleChange}
            />
          </div>
          <div className="col-12 my-4">
            <button
              style={{ borderRadius: 20, backgroundColor: "#25b79b" }}
              type="submit"
              className="btn btn-block w-25 btn-success mx-auto"
            >
              ثبت نام
            </button>
          </div>
        </Row>
      </Form>
    </Container>
  );
};

export default RegisterForm;
