import LastInsuranceForm from "./Form";
import LastInsuranceFormMobile from "./FormMobile";

export { LastInsuranceForm, LastInsuranceFormMobile };
