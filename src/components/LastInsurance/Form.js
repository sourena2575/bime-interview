import React from "react";
import { Row, Col } from "reactstrap";
import SubmitForm from "./SubmitForm";
import { LeftIcon, RightIcon } from "../common/Icons";

const Form = ({ data, history }) => {
  return (
    <SubmitForm>
      {({ company, setcompany, dispatch }) => (
        <>
          <h5 className="font-weight-bold text-right py-4">بیمه شخص ثالث</h5>
          <h6 className="text-right text-muted pb-4">
            شرکت بیمه گر قبلی خود را در این قسمت انتخاب کنید
          </h6>
          <Row>
            <Col xs="12">
              <select
                className="form-control"
                style={{ direction: "rtl" }}
                onChange={(e) => setcompany(e.target.value)}
              >
                <option value="">شرکت بیمه گر قبلی </option>
                {data.map((item) => {
                  return (
                    <option key={item.id} value={item.company}>
                      {item.company}
                    </option>
                  );
                })}
              </select>
            </Col>
          </Row>
          <Row className="my-5">
            <Col xs="6">
              <button
                className="w-50 ml-auto btn btn-outline-info"
                style={{
                  borderColor: "##25b79b",
                  color: "#25b79b",
                  borderRadius: 20,
                  borderWidth: 1,
                }}
                onClick={() => {
                  dispatch({ type: "COMPANY", payload: { company } });
                  if (company) {
                    history.push("/discount");
                  }
                }}
              >
                <LeftIcon />
                مرحله بعد
              </button>
            </Col>
            <Col xs="6">
              <button
                className="w-50 float-right btn btn-outline-info"
                style={{
                  borderColor: "##25b79b",
                  color: "#25b79b",
                  borderRadius: 20,
                  borderWidth: 1,
                }}
                onClick={() => history.push("/type")}
              >
                بازگشت
                <RightIcon />
              </button>
            </Col>
          </Row>
        </>
      )}
    </SubmitForm>
  );
};

export default Form;
