import { useContext, useState } from "react";
import { MainContext } from "../../context/ContextProvider";

const RenderProps = ({ children }) => {
  const { dispatch } = useContext(MainContext);
  const [company, setcompany] = useState("");

  return children({
    company: company,
    setcompany: setcompany,
    dispatch: dispatch,
  });
};

export default RenderProps;
