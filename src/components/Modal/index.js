import React, { useContext } from "react";
import { Modal, Container, Row, Col } from "reactstrap";
import { MainContext } from "../../context/ContextProvider";
const Index = () => {
  const { state, dispatch } = useContext(MainContext);

  return (
    <Modal isOpen={state.modal}>
      <Container className="my-5">
        <Row>
          <Col xs="12">
            <p className="text-center py-4 font-weight-bold">اطلاعات وارده </p>
          </Col>
          <Col
            xs="12"
            className="d-flex flex-row-reverse justify-content-between px-5"
          >
            <p className="text-right ">: نام </p>
            <p className="text-left font-weight-bold">{state.user.name}</p>
          </Col>
          <Col
            xs="12"
            className="d-flex flex-row-reverse justify-content-between px-5"
          >
            <p className="text-right ">: نام خانوادگی </p>
            <p className="text-left font-weight-bold">{state.user.last_name}</p>
          </Col>
          <Col
            xs="12"
            className="d-flex flex-row-reverse justify-content-between px-5"
          >
            <p className="text-right ">: شماره موبایل </p>
            <p className="text-left font-weight-bold">{state.user.phone}</p>
          </Col>
          <Col
            xs="12"
            className="d-flex flex-row-reverse justify-content-between px-5"
          >
            <p className="text-right ">: نوع خودرو </p>
            <p className="text-left font-weight-bold">{state.car}</p>
          </Col>
          <Col
            xs="12"
            className="d-flex flex-row-reverse justify-content-between px-5"
          >
            <p className="text-right ">: مدل خودرو </p>
            <p className="text-left font-weight-bold">{state.carModel}</p>
          </Col>
          <Col
            xs="12"
            className="d-flex flex-row-reverse justify-content-between px-5"
          >
            <p className="text-right ">: شرکت بیمه گر قبلی </p>
            <p className="text-left font-weight-bold">{state.company}</p>
          </Col>
          <Col
            xs="12"
            className="d-flex flex-row-reverse justify-content-between px-5"
          >
            <p className="text-right ">: درصد تخفیف ثالث </p>
            <p className="text-left font-weight-bold">{state.discount}</p>
          </Col>
          <Col
            xs="12"
            className="d-flex flex-row-reverse justify-content-between px-5"
          >
            <p className="text-right ">: درصد تحفیف حوادث راننده </p>
            <p className="text-left font-weight-bold">
              {state.discountAccident}
            </p>
          </Col>
          <Col xs="12 my-5">
            <button
              style={{ borderRadius: 20, backgroundColor: "#25b79b" }}
              type="submit"
              className="btn btn-block w-50 btn-success mx-auto"
              onClick={() => dispatch({ type: "MODAL" })}
            >
              بازگشت
            </button>
          </Col>
        </Row>
      </Container>
    </Modal>
  );
};

export default Index;
