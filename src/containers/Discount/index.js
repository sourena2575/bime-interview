import React, { useState, useEffect } from "react";
import { Container, Col, Row } from "reactstrap";
import { Breakpoint } from "react-socks";
import { DiscountForm, DiscountFormMobile } from "../../components/Discount";
import { getDiscount } from "../../services";
import Modal from "../../components/Modal";
const Index = () => {
  const [discount, setdiscount] = useState([]);
  useEffect(() => {
    const getCarDiscount = async () => {
      await getDiscount().then((res) => {
        //console.log(res);
        setdiscount(res.result);
      });
    };
    getCarDiscount().catch((err) => console.log(err));
  }, []);
  console.log(discount);
  if (!discount.length) {
    return null;
  }
  return (
    <>
      <Container className="my-5 d-none d-md-block">
        <Row>
          <Col xs="6"></Col>
          <Col xs="6">
            <DiscountForm data={discount} />
          </Col>
        </Row>
      </Container>
      <Breakpoint small down>
        <Container className="my-5">
          <Row>
            <Col xs="12">
              <DiscountFormMobile data={discount} />
            </Col>
          </Row>
        </Container>
      </Breakpoint>
      <Modal />
    </>
  );
};

export default Index;
