import React from "react";
import { Container, Row, Col } from "reactstrap";
import { Breakpoint } from "react-socks";
import {
  RegisterForm,
  RegisterFormMobile,
} from "../../../components/Authentication/Register";

const index = ({ history }) => {
  return (
    <>
      <Container className="my-5 d-none d-md-block">
        <Row>
          <Col xs="6"></Col>
          <Col xs="6" className="my-5">
            <RegisterForm history={history} />
          </Col>
        </Row>
      </Container>
      <Breakpoint small down>
        <Container className="my-5 ">
          <Row>
            <Col xs="12">
              <RegisterFormMobile history={history} />
            </Col>
          </Row>
        </Container>
      </Breakpoint>
    </>
  );
};

export default index;
