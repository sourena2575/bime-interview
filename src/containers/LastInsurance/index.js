import React, { useState, useEffect } from "react";
import { Container, Col, Row } from "reactstrap";
import { Breakpoint } from "react-socks";
import {
  LastInsuranceForm,
  LastInsuranceFormMobile,
} from "../../components/LastInsurance";
import { getCompanies } from "../../services";

const Index = ({ history }) => {
  const [companies, setcompanies] = useState([]);
  useEffect(() => {
    const getinsuranceCompanies = async () => {
      await getCompanies().then((res) => {
        //console.log(res);
        setcompanies(res.result);
      });
    };
    getinsuranceCompanies().catch((err) => console.log(err));
  }, []);
  if (!companies.length) return null;
  return (
    <>
      <Container className="my-5 d-none d-md-block">
        <Row>
          <Col xs="6"></Col>
          <Col xs="6">
            <LastInsuranceForm data={companies} history={history} />
          </Col>
        </Row>
      </Container>
      <Breakpoint small down>
        <Container className="my-5">
          <Row>
            <Col xs="12">
              <LastInsuranceFormMobile data={companies} history={history} />
            </Col>
          </Row>
        </Container>
      </Breakpoint>
    </>
  );
};

export default Index;
