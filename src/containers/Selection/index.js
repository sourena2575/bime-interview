import React from "react";
import { Container, Col, Row } from "reactstrap";
import { Breakpoint } from "react-socks";
import { Select, SelectMobile } from "../../components/Selection";

const Index = ({ history }) => {
  return (
    <>
      <Container className="my-5 d-none d-md-block">
        <Row>
          <Col xs="6"></Col>
          <Col xs="6">
            <Select history={history} />
          </Col>
        </Row>
      </Container>
      <Breakpoint small down>
        <Container className="my-5">
          <Row>
            <Col xs="12">
              <SelectMobile history={history} />
            </Col>
          </Row>
        </Container>
      </Breakpoint>
    </>
  );
};

export default Index;
