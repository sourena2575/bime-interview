import React, { useEffect, useState } from "react";
import { Container, Col, Row } from "reactstrap";
import { Breakpoint } from "react-socks";
import { CarTypeForm, CarTypeFormMobile } from "../../components/CarType";
import { getTypes } from "../../services";

const Index = ({ history }) => {
  const [types, settypes] = useState([]);

  useEffect(() => {
    const getCarTypes = async () => {
      await getTypes().then((res) => {
        settypes(res.result);
      });
    };
    getCarTypes().catch((err) => console.log(err));
  }, []);
  if (!types.length) {
    return null;
  }

  return (
    <>
      <Container className="my-5 d-none d-md-block">
        <Row>
          <Col xs="6"></Col>
          <Col xs="6">
            <CarTypeForm types={types} history={history} />
          </Col>
        </Row>
      </Container>
      <Breakpoint small down>
        <Container className="my-5">
          <Row>
            <Col xs="12">
              <CarTypeFormMobile types={types} history={history} />
            </Col>
          </Row>
        </Container>
      </Breakpoint>
    </>
  );
};

export default Index;
