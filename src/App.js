import React from "react";
import { BreakpointProvider } from "react-socks";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import { NavbarResponsive } from "./components/Navbar";
import Register from "./containers/Authentication/Register";
import CarType from "./containers/CarType";
import LastInsurance from "./containers/LastInsurance";
import Discount from "./containers/Discount";
import Select from "./containers/Selection";

function App() {
  return (
    <BreakpointProvider>
      <BrowserRouter>
        <NavbarResponsive />
        <Switch>
          <Route exact path="/" component={Register} />
          <Route exact path="/select" component={Select} />
          <Route exact path="/type" component={CarType} />
          <Route exact path="/company" component={LastInsurance} />
          <Route exact path="/discount" component={Discount} />
        </Switch>
      </BrowserRouter>
    </BreakpointProvider>
  );
}

export default App;
