import React from "react";
import { Spin } from "antd";
const Spiner = () => {
  return (
    <div className="d-flex align-items-center justify-content-center my-5 py-5">
      <Spin size="large" />
    </div>
  );
};

export default Spiner;
